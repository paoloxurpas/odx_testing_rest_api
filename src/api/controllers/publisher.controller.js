const httpStatus = require('http-status');
const Publisher = require('../models/publisher.model');
const { handler: errorHandler } = require('../middlewares/error');
const { omit } = require('lodash');

/**
 * Load user and append to req.
 * @public
 */
exports.load = async (req, res, next, id) => {
  try {
    const publisher = await Publisher.get(id);
    req.locals = { publisher };
    return next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

/**
 * Get user
 * @public
 */
exports.get = (req, res) => res.json(req.locals.publisher.transform());

/**
 * Get logged in user info
 * @public
 */
exports.loggedIn = (req, res) => res.json(req.publisher.transform());

/**
 * Get user
 * @public
 */
exports.get = (req, res) => res.json(req.locals.publisher.transform());

/**
 * Create new user
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const publisher = new Publisher(req.body);
    const savedPublisher = await publisher.save();
    res.status(httpStatus.CREATED);
    res.json(savedPublisher.transform());
  } catch (error) {
    next(error)
  }
};

/**
 * Replace existing user
 * @public
 */
exports.replace = async (req, res, next) => {
  try {

    const { publisher } = req.locals;
    const newPublisher = new Publisher(req.body);
  //  const ommitRole = user.role !== 'admin' ? 'role' : '';
    const newPublisherObject = omit(newPublisher.toObject(), '_id'); //, ommitRole);

    await publisher.update(newPublisherObject, { override: true, upsert: true });
    const savedPublisher = await Publisher.findById(publisher._id);

    res.json(savedPublisher.transform());

  } catch (error) {
    next(error);
  }
};

/**
 * Update existing user
 * @public
 */
exports.update = (req, res, next) => {

//  const ommitRole = req.locals.user.role !== 'admin' ? 'role' : '';
//  const updatedUser = omit(req.body, ommitRole);
//  const user = Object.assign(req.locals.user, updatedUser);

  const publisher = Object.assign(req.locals.publisher, req.body);

  publisher.save()
    .then(savedPublisher => res.json(savedPublisher.transform()))
    .catch(e => next(e));

};

/**
 * Get user list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const publishers = await Publisher.list(req.query);
    const transformedPublishers = publishers.map(publisher => publisher.transform());
    res.json(transformedPublishers);
  } catch (error) {
    next(error);
  }
};

/**
 * Delete user
 * @public
 */
exports.remove = (req, res, next) => {
  const { publisher } = req.locals;

  publisher.remove()
    .then(() => res.status(httpStatus.NO_CONTENT).end())
    .catch(e => next(e));
};
